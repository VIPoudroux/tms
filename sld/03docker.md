---
marp: true
paginate: true
# backgroundImage: url('https://marp.app/assets/hero-background.svg')
class: slide
theme: esiroi
---


<style>
    table {
        font-size:0.7em;
        margin-left:auto;
        margin-right:auto;
    }
    .columns {
        display: grid;
        grid-template-columns: var(--split);
        gap: 0.5rem;
    }

    .column_center {
        display: flex;
        align-items: center;
    }
    header {
        width:95%;
        text-align:center;
        font-size:0.8em;
    }
    section.slide {
        display: block;
    }

    strong {
        color: #19618e;
    }
    .small{
        font-size: 0.8em;
    }

    section.title {
        display: flex;
        text-align:center;
    }

    .bloc 
    {
        background-color: #eee;
        padding: 10px;
        margin: 10px;
        border-radius: 5px;
        font-size: 1em;
    }

    .bloc > h3, div
    {
        margin: 0px;
        margin-left: 10px;
    }

    .bloc > hr
    {
        padding: 0px;
        margin: 0px;
        margin-top: 5px;
        margin-bottom: 10px;
        color: #fff;
        height:2px;
    }

    img[alt~="center"] {
        display: block;
        margin: 0 auto;
    }

    .image_caption{
        text-align:center;
        font-size: 0.7em;
        font-style: italic;
    }
</style>


<!-- _class: title -->
![bg left:40% 80%](img/docker01.png)
# ***Outils pour l'ingénieur***
## docker
Tahiry Razafindralambo - Semestre 1 - 2023

---
<!-- header: outils pour l'ingénieur -->
<!-- _class: title -->
# ***les concepts : <br> machines virtuelles et containeurs***





---
# Machines virtuelles (1)

- une machine virtuelle (VM) et une simulation d'un ordinateur
- vous configurez le système d'exploitation invité et l'utilisez sans affecter la machine hôte. 
- on utilise les VM pour tester des systèmes d'exploitation, des logiciels et/ou des configurations sans "risque" pour le système hôte
- en général on utilse les VMs pour utiliser des logiciels ou programmes qui ne tournent que dans un système d'exploitation spécifique. 
- on utilise les VM pour tester des vulnérabilités ou des programmes malicieux.


---
# Machines virtuelles (2)

***avantages***

- isolation :les hyperviseurs permettent de relativement bien isoler les systèmes invités des systèmes hôtes. 
- snapshots : on peut prendre un spnashot d'une machine virtuelle (disque, mémoire, etc..) faire des changements sur la machine et la restaurer dans un état sauvegardé. 

***inconvénients***

une machine virtuelle est souvent plus lente qu'une installation direct. 

---
# Machines virtuelles (3)


***foncionnement***
- ressources : partagées avec l'hôtes. Attention quand vous allouez les ressources
- réseau : plusieurs options : NAT (par défaut), accès par pont (pour apparaitre directement sur le réseau ou se trouve l'hôte), réseau interne (réseau spécifique entre l'hôte et l'invité)
- autres : il existe des logiciels permettant une meilleur intégration en l'hôtes et l'invité.  

---
# Machines virtuelles (4)

***hyperviseur***

- VirtualBox (open-source)
- Virt-manager (open-source - KVM et LXC)
- VMWare (commercial)


---
<!-- header: outils pour l'ingénieur -->
<!-- _class: title -->
# ***les containeurs***



---

# l'origine

<div class="columns" style="--split: 2fr 1fr ">
<div>

- les machines virtuelles sont assez lourdes
- les conteneurs permettent d'avoir rapidement une "machine"
- les conteneurs permettent d'automatiser "facilement" la création des machines

***les logiciels***

- docker
- lxc
- jail
- rkt, amazon firecracker, ...

</div><div>

![](img/docker02.png)

</div>
---


# description
 
- les conteneurs sont "simplement" un "assemblage" des ***fonctions de sécurité*** de linux (système de fichier virtuel, interface réseau virtuelle, chroots, memoire virtuelle, ...) avec une ***apparence*** de machine virtuelle.

- les conteneurs en sont pas aussi sécurisés et isolés que les VMs mais ça s'améliore. 

- les conteneurs sont souvent plus performante et plus rapide à démarrer qu'une VM, mais pas toujours.

- pour les performances, contrairement aux VMs qui utulisent une copie entiere d'un système, le conteneur partage le même noyau que l'hôte.

- si vous voulez un conteneur linux sous mac ou windows ou autre, il vus faudra une VM linux.

---

# utilisation
 
Les conteneurs sont utiles quand vous voulez automatiser une tâche de manière standardisée (par exemple pour) : 

- des serveurs pré-packagé
- un système de build
- un environnement de développement
- lancer des programmes non surs
- faire de l'intégration continu
<br>
- docker est beaucoup utilisé pour les problèmes de dépendances

---

# comment 
 
- on ecrit un fichier qui permet de définir la construction du conteneur.
- on commence par une image minimal (comme Alpine Linux)
- on donne une liste de commande à executer pour mettre en place le système (package à installer, fichier à copier, fichier de configuration, ...)
- on spécifie aussi tous les ports externes qui doivent être disponibles / visibles
- on définit un point d'entrée qui indique quelle commande doit être exécutée quand le conteneur démarre.
- il existe des dépôts qui contient des images contenant des services pré-installé et pré-configurés

---


# Références bibliographiques

Livres accessibles en ligne sur le site de la BU :
* https://docker.com, 2023
