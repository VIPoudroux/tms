---
marp: true
paginate: true
# backgroundImage: url('https://marp.app/assets/hero-background.svg')
class: slide
theme: esiroi
---

<!-- _class: title -->
![bg left:40% 80%](img/git06.png)
# ***Outils pour l'ingénieur***
## git (suite)
Tahiry Razafindralambo - Semestre 1 - 2023

---
<!-- header: outils pour l'ingénieur -->
<!-- _class: title -->
# ***Quelques rappels***




<style>
    table {
        font-size:0.7em;
        margin-left:auto;
        margin-right:auto;
    }
    .columns {
        display: grid;
        grid-template-columns: var(--split);
        gap: 0.5rem;
    }

    .column_center {
        display: flex;
        align-items: center;
    }
    header {
        width:95%;
        text-align:center;
        font-size:0.8em;
    }
    section.slide {
        display: block;
    }

    strong {
        color: #19618e;
    }
    .small{
        font-size: 0.8em;
    }

    section.title {
        display: flex;
        text-align:center;
    }

    .bloc 
    {
        background-color: #eee;
        padding: 10px;
        margin: 10px;
        border-radius: 5px;
        font-size: 1em;
    }

    .bloc > h3, div
    {
        margin: 0px;
        margin-left: 10px;
    }

    .bloc > hr
    {
        padding: 0px;
        margin: 0px;
        margin-top: 5px;
        margin-bottom: 10px;
        color: #fff;
        height:2px;
    }

    img[alt~="center"] {
        display: block;
        margin: 0 auto;
    }

    .image_caption{
        text-align:center;
        font-size: 0.7em;
        font-style: italic;
    }
</style>


---
# GIT : un peu de philosophie (1)

<div class="columns" style="--split: 2fr 1fr ">
<div>

- git sauvegarde un instantanné de tout le dépôt à chaque commit
- git enregistre une référence de cet instantanné 
- git ne fait pas de diff
- pour être efficace, sur les fichiers qui n'ont pas change, git ne stocke pas le fichier à nouveau, mais une référence vers le fichier original. 

</div>
<div>  

![](img/git08.png)

<br>

![](img/git07.png)

</div>
</div>

---
# GIT : un peu de philosophie (2)



- dans git, les opérations sont locales (historique par exemple)
- git gère l'intégrité (checksum)
- git ajoute les données, mais ne les supprimes pas
- avec git soit un fichier est suivi (connu par git), soit il ne l'est pas


---

# GIT : les 3 états


<div class="columns" style="--split: 2fr 1fr ">
<div>

1. modifié (dans votre caddie) : vous modifiez des fichiers dans votre répertoire de travail ;
1. indéxé (sur le tapis de caisse) : vous indexez les fichiers modifiés, ce qui ajoute des instantanés de ces fichiers dans la zone d’index ;
1. validé (payé avec ticket de caisse) vous validez, ce qui a pour effet de basculer les instantanés des fichiers de l’index dans la base de données du répertoire Git.

</div>
<div>  

![](img/git09.png)


</div>
</div>


---


# Rappels des commandes de base
 
```
tah@dev $ git init (demarre un dépôtr git dans le repertoire courrant)
tah@dev $ git clone https://url (copie un dépot distant localement)
 ```

```

tah@dev $ git add fichier (ajoute fichier à l'index)
tah@dev $ git commit (sauvegarde les modification de l'index dans un commit)
tah@dev $ git push (envoi les modifications locales vers le dépôt distant)
tah@dev $ git status (voir l'état de votre dépôt)
 ```

---

<!-- _class: title -->
# ***Quelques commandes utiles (exemple)***
---

# exemple (1) : action de base
 
```bash
tah@dev $ git clone https://trazafin@gitlab.com/trazafin/test.git
 [...]

tah@dev $ cd test
tah@dev $ git status
 Sur la branche main
 Votre branche est à jour avec 'origin/main'.

 rien à valider, la copie de travail est propre
tah@dev $ echo "test git" > README.md
tah@dev $ git status
 Sur la branche main
 Votre branche est à jour avec 'origin/main'.

 Fichiers non suivis:
  (utilisez "git add <fichier>..." pour inclure dans ce qui sera validé)
        README.md

 aucune modification ajoutée à la validation mais 
 des fichiers non suivis sont présents (utilisez "git add" pour les suivre)

tah@dev $ git add README
tah@dev $ git status
 Sur la branche main
 Votre branche est à jour avec 'origin/main'.

 Modifications qui seront validées :
  (utilisez "git restore --staged <fichier>..." pour désindexer)
  (utilisez "git reset HEAD <fichier>..." pour désindexer)
        nouveau fichier : README.md
```

---

# exemple (2) : indexation 
 
```bash
tah@dev $ echo "ligne 5" >> fic.txt
tah@dev $ git status
 Sur la branche main
 Votre branche est à jour avec 'origin/main'.

 Modifications qui seront validées :
  (utilisez "git restore --staged <fichier>..." pour désindexer)
        nouveau fichier : README.md

 Modifications qui ne seront pas validées :
  (utilisez "git add <fichier>..." pour mettre à jour ce qui sera validé)
  (utilisez "git restore <fichier>..." pour annuler les modifications dans le répertoire de travail)
        modifié :         fic.txt

tah@dev $ git add fic.txt 
tah@dev $ git status
 Sur la branche main
 Votre branche est à jour avec 'origin/main'.

 Modifications qui seront validées :
  (utilisez "git restore --staged <fichier>..." pour désindexer)
        nouveau fichier : README.md
        modifié :         fic.txt
tah@dev $ git status -s
 A  README.md
 M  fic.txt

```

---

# exemple (3) : commit et log
 
```bash
tah@dev $ git commit || git commit -m "message court"
 [message]
tah@dev $ git rm (supprime un fichier du suivi git)
tah@dev $ git mv README.md LISEZMOI.md (renomme un fichier au sens de git)

tah@dev $ mv README.md LISEZMOI.md
tah@dev $ git rm README.md
tah@dev $ git add LISEZMOI.md

tah@dev $ git log

```

---

# exemple (4) : annulation des actions
 
```bash
tah@dev $ git commit --amend
 rectifie le dernier commit

tah@dev $ git reset HEAD fic.txt
 de-indexe un fichier indéxé

tah@dev $ echo "ligne 6" >> fic.txt
tah@dev $ git status
 Sur la branche main
 Votre branche est en avance sur 'origin/main' de 1 commit.
  (utilisez "git push" pour publier vos commits locaux)

 Modifications qui ne seront pas validées :
  (utilisez "git add <fichier>..." pour mettre à jour ce qui sera validé)
  (utilisez "git restore <fichier>..." pour annuler les modifications dans le répertoire de travail)
        modifié :         fic.txt

aucune modification n'a été ajoutée à la validation (utilisez "git add" ou "git commit -a")

tah@dev $ git checkout -- fic.txt || git restore fic.txt
 re-initialise les modifications
```

---

<!-- _class: title -->
# ***les branches***
---

# intuitions (1)

- Lors d'un commit, git stocke un objet `commit` qui contient un pointeur vers l'instantané (`snapshot`)
- cet objet `commit`  contient aussi les informations du développeur, le message, et des pointeurs vers le ou les `commits` qui précèdent directement le `commit`. 
- aucun parent pour le commit initial
- un parent pour un commit normal
- plusieurs parents pour un commit provenant de plusieurs "branches".



---

# intuitions (2)
<div class="columns" style="--split: 1fr 1fr ">
<div>

- supposons que vous avez un répertoire avec 3 fichiers. 
- l'indexation des fichiers calcule une empreinte (`checksum`) pour chaque fichier
- stocke cette version dans le dépôt (appelé `blob`)
- ajoute cette empreinte dans à la zone d'index

</div><div>

![](img/git10.png)

</div>

---


# intuitions (3)
<div class="columns" style="--split: 1fr 1fr ">
<div>

- Le prochain commit stocke un pointeur vers le commit précédent. 
- une branche dans git est un pointeur que l'on déplace entre commits
- la branche par défaut de git s'appelle `main` (master avant)
- au fil des commits, la branche `main` pointe vers le dernier commit
- le pointeur de la branche `main` avance automatique.

</div><div>

![](img/git11.png)

</div>

---

# une branche (1)

<div class="columns" style="--split: 1fr 1fr ">
<div>

```
tah@dev $ git branch testing
```

git conserve un pointeur spécial `head` qui est le pointeur sur la branche locale

```
tah@dev $ git log --oneline --decorate
    b6ab905 (HEAD -> main, testing) test
    2c984b8 (origin/main, origin/HEAD) ajoute la ligne 4
    [...]
```

pour changer de branche

```
tah@dev $ git switch testing || git checkout testing
    Basculement sur la branche 'testing'
tah@dev $ git log --oneline --decorate
    b6ab905 (HEAD -> testing, main) test
    2c984b8 (origin/main, origin/HEAD) ajoute la ligne 4
```

</div><div>

![](img/git12.png)

</div>

---
# une branche (2)


```
tah@dev $ echo "ligne 7 testing" >> fic.txt
tah@dev $ git commit -am "add line 7 testing"
    [testing d68bd08] add line 7 testing
    1 file changed, 1 insertion(+)
tah@dev $ git switch main 
    Basculement sur la branche 'main'
tah@dev $ echo "ligne 7" >> fic.txt
tah@dev $ git commit -am "add ligne 7"
    [main 43a9c3f] add ligne 7
    1 file changed, 1 insertion(+)
tah@dev $ git log --oneline --decorate --graph --all
    * 43a9c3f (HEAD -> main) add ligne 7
    | * d68bd08 (testing) add line 7 testing
    |/
    * 4e3e1a2 add line 6
    * b6ab905 test
```

---


# une branche (3)


```
tah@dev $ git switch -c bug123
    Basculement sur la nouvelle branche 'bug123'
tah@dev $ (echo "ligne 0"; cat fic.txt)  > /tmp/t ; mv /tmp/t fic.txt
tah@dev $ git commit -am "resolve bug123, add ligne 0"
    [bug123 6a07d03] resolve bug123, add ligne 0
    1 file changed, 1 insertion(+)
tah@dev $ git switch main
tah@dev $ git log --oneline --decorate --graph --all
    * 6a07d03 (bug123) resolve bug123, add ligne 0
    * 43a9c3f (HEAD -> main) add ligne 7
    | * d68bd08 (testing) add line 7 testing
    |/
    * 4e3e1a2 add line 6
    * b6ab905 test

```

---


# une branche (4)


```
tah@dev $ git merge bug123 
    Mise à jour 43a9c3f..6a07d03
    Fast-forward
    fic.txt | 1 +
    1 file changed, 1 insertion(+)
tah@dev $ git branch -d bug123
    Branche bug123 supprimée (précédemment 6a07d03).
tah@dev $ git switch testing
    Basculement sur la branche 'testing'
tah@dev $ echo "ligne 8 testing" >> fic.txt
tah@dev $ git commit -am "add line 8"
    [testing 1104a52] add line 8
    1 file changed, 1 insertion(+)
```

---

# une branche (5)


```
tah@dev $ git switch main
tah@dev $ git merge testing
    Fusion automatique de fic.txt
    CONFLIT (contenu) : Conflit de fusion dans fic.txt
    La fusion automatique a échoué ; réglez les conflits et validez le résultat.
tah@dev $ vim fic.txt
    (résolution)
tah@dev $ git status
    Vous avez des chemins non fusionnés.
        (réglez les conflits puis lancez "git commit")
        (utilisez "git merge --abort" pour annuler la fusion)

    Chemins non fusionnés :
    (utilisez "git add <fichier>..." pour marquer comme résolu)
        modifié des deux côtés :  fic.txt
tah@dev $ git add fic.txt
tah@dev $ git commit 
    Merge branch 'testing'

    # Conflicts:
    #       fic.txt
    [...]

```

---

# une branche (6)


```
tah@dev $ git branch 
tah@dev $ git branch -v
tah@dev $ git branch --merged
tah@dev $ git branch --no-merged
tah@dev $ git branch --move <ancienNom> <nouveauNom>
tah@dev $ git push --set-upstream origin <nouveauNom>
tah@dev $ git push origin --delete <ancienNom>
```

---


# une branche (7) : merge / rebase

<div class="columns" style="--split: 2fr 1fr ">
<div>

- `merge` realise une fusion à trois branche (branche actuelle, distante, ancetre)
- `merge` crée un nouvel instantané a partir de cette fusion


```
tah@dev $ 
```

</div><div>

![](img/git13.png)
![](img/git14.png)


</div>

---

# une branche (8) : merge / rebase

<div class="columns" style="--split: 2fr 1fr ">
<div>


- `rebase` re-applique les modifications

```
tah@dev $ git switch experiment
tah@dev $ git rebase master
tah@dev $ git switch master
tah@dev $ git merge experiment
```

</div><div>

![](img/git13.png)
![](img/git15.png)
![](img/git16.png)

</div>

---


# Références bibliographiques

Livres accessibles en ligne sur le site de la BU :
* "Pro Git book", Scott Chacon and Ben Straub, Apress, 2014
