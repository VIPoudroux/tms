---
title:
- Outils pour l'ingénieur \newline gestion de version de code
author:
- tahiry razafindralambo
theme:
- metropolis
date:
- Semestre 1, 2023
aspectratio: 
- 169
fontsize: 
- 12pt
slide-level: 
- 2
header-includes: |
    \setbeamercolor{frametitle}{bg=black,fg=white} 

---

# informations administratives


## L'objectif global de ce cours {.t}

- Une présentation de quelques outils
- Une présentation de l'utilisation de certain outils
- Une présentation de certain concept (git, virtualisation ... )
- Une présentation des certaines "bonnes" pratiques


## ce que nous allons faire aujourd'hui {.t}

- Une introduction à git et github / gitlab


## les évaluations

- QCM
- du code
- ...


# ce que vous devez retenir

## ce n'est pas toujours facile

- Les courbes d'apprentissage ne sont pas simples
- Il faut utiliser les outils
- Il faut comprendre les concepts

## l'efficacité est sans limite ... 

- Ce cours doit vous permettre d'être efficace
- Il faut accepter d'apprendre




# La gestion de version

## pourquoi / pour quoi git existe... 

- contrôle de version
- collaboration

C'est un système qui permet de conserver tout l'historique d'un projet, mais qui permet aussi à plusieurs personnes de travailler sur le même projet de manière concurrente, distribuée et collaborative.

## git et github

- git est le logiciel qui permet de faire du contrôle de version
- github (ou gitlab) est un webservice (siteweb, réseau social) qui fait tourner git en arrière plan

Vous pouvez utiliser git sans github, l'inverse est plus difficile.


## quel est notre problème 

![](img/i001.png){ height=160px}
![](img/i002.png){ height=160px}

Comment est-ce que vous gérer vos fichiers, et leurs versions successives ?

## les possibilités de git (1)

![](img/git01.png){ width=250px}

garder les versions et la progression des modifications dans les fichiers

## les possibilités de git (2)

![](img/git02.png){ width=250px}

collaborer à plusieurs sur un même projet


## les possibilités de git (3)

![](img/git03.png){ width=250px}

tester des fonctionnalités sans altérer les fichiers principaux


## les possibilités de git (4)

![](img/git04.png){ width=250px}

revenir sur des versions précédentes des fichiers


# git les commandes de bases

## github

- installer git (mac, windows, linux)
- créer un compte sur github ou gitlab
- créer un dépot / repository (une sorte de répertoire)

(live demo)

## commandes : git clone

\tiny
::: columns
:::: {.column width=.4}


Dans votre terminal

\vspace{.5cm}

```bash
$ git clone https://gitlab.com/trazafin/hello
(...)
$ ls
hello
$ cd hello
$ ls


```
\vspace{0cm}

![](img/git05.png){ width=150px}

::::

:::: {.column width=.6}
- c'est une commande qui permet de "copier" un dépot depuis github ou gitlab, sur votre machine locale



::::
:::
\normalsize



## commandes : git status 

\tiny
::: columns
:::: {.column width=.4}


Dans votre terminal

\vspace{.5cm}

```bash
$ git status
Sur la branche master

Aucun commit

rien à valider (créez/copiez des fichiers et 
utilisez "git add" pour les suivre)
$ touch fichier.txt
[ajout contenu]
$ git status
[...]
Fichiers non suivis:
  (utilisez "git add <fichier>..." pour inclure dans ce qui sera validé)
	fichier.txt

[...]


```

::::

:::: {.column width=.6}
- `git status` permet de voir le statut de votre dépot. Si vous ne savez pas vraiment quoi faire, cette commande vous le dira.



::::
:::
\normalsize

## commandes : git add 

\tiny
::: columns
:::: {.column width=.4}


Dans votre terminal

\vspace{.5cm}

```bash
$ git add fichier.txt
$ git status
Modifications qui seront validées :
  (utilisez "git rm --cached <fichier>..." pour désindexer)
	nouveau fichier : fichier.txt


```

::::
:::: {.column width=.6}
- `git add` permet d'ajouter un ou plusieurs fichiers ou dossiers à l'index (stage).
- un fichier indéxé n'est pas encore "sauvegardé" dans git.
- l'index est une étape intermédiaire avant la "sauvegarde" (ou commit)
- si vous travaillez sur plusieurs fichiers, `git add` permet de pré-selectionner les fichiers et l'états des fichiers que vous voulez sauvegarder. 
- cette sélection permet d'avoir des sauvagardes dans des états "propres".

\vspace{1cm}


- `git status` permet de voir le statut de votre dépot. Si vous ne savez pas vraiment quoi faire, cette commande vous le dira.



::::
:::
\normalsize


## commandes : git commit 

\tiny
::: columns
:::: {.column width=.4}


Dans votre terminal

\vspace{.5cm}

```bash
$ git commit
[...] 
[master (commit racine) d5fd7f3] titre du commit
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 fichier.txt
$ git status
Sur la branche master
rien à valider, la copie de travail est propre


```

::::
:::: {.column width=.6}
- `git commit` permet de sauvegarder votre dépot.
- le commit sauvegarde les modifications faites dans les fichiers indéxés
- un commit est obligatoirement accompagné d'un message. 
- ce message est important. Il explique ce que fait le commit. 
- ce message permet aussi de reconnaitre les commits.


::::
:::
\normalsize


## commandes : git push 

\tiny
::: columns
:::: {.column width=.4}


Dans votre terminal

\vspace{.5cm}

```bash
$ git push
[...] 
$ git pull
(live demo)

```

::::
:::: {.column width=.6}
- `git push` permet de "pousser" vos modifications vers github ou gitlab
- `git pull` permet de "récupérer" les modifications depuis github ou gitlab si plusieurs personnes travaillent sur le même projet ou si vous travailler depuis plusieurs postes différents par exemple. 

\vspace{1cm}
- ATTENTION : il peut y avoir des conflits dans les modifications. Dans la plupart des cas, git les résoud pour vous. 
- les conflits apparaissent (la plupart du temps) si une même ligne a été modifiée
- dans ce cas, c'est à l'utilisateur de resoudre le conflit et de créer un commit avec le bon état.

::::
:::
\normalsize

## résolution de conflits 

\tiny
::: columns
:::: {.column width=.4}


Dans votre terminal

\vspace{.5cm}

```bash
(live demo)
```

::::
:::: {.column width=.6}
-
::::
:::
\normalsize
