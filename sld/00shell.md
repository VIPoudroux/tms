---
title:
- Outils pour l'ingénieur \newline La ligne de commande
author:
- tahiry razafindralambo
theme:
- metropolis
date:
- Semestre 1, 2023
aspectratio: 
- 169
fontsize: 
- 12pt
slide-level: 
- 2
header-includes: |
    \setbeamercolor{frametitle}{bg=black,fg=white} 

---

# informations administratives


## L'objectif global de ce cours {.t}

- Une présentation de quelques outils
- Une présentation de l'utilisation de certain outils
- Une présentation de certain concept (git, virtualisation ... )
- Une présentation des certaines "bonnes" pratiques


## ce que nous allons faire aujourd'hui {.t}

- Une introduction à la ligne de commande


## les évaluations

- QCM
- du code
- ...


# ce que vous devez retenir

## ce n'est pas toujours facile

- La courbes d'apprentissage n'est pas simple
- Il faut utiliser les outils
- Il faut comprendre les concepts

## l'efficacité est sans limite ... 

- Ce cours doit vous permettre d'être efficace
- Il faut accepter d'apprendre

\normalsize



# La ligne de commande

## Shell

- une manière d'interagir avec votre ordinateur
- une manière efficace pour les taches répétitives
- bien mieux que l'interfaces graphique
- permet une "composition" des commandes / programmes
- permet d'automatiser les tâches


## Shell

- toutes les plateformes fournissent une sorte de "shell"
- windows (powershel), mac (bash), linux (bash), android (sh), ... 

- Dans ce cours nous allons utiliser le shell bash sous linux (ou mac)

- installez un shell dans votre OS préféré (WSL par exemple)



## bash

![](img/00iconshell.png)

`Bash` (acronyme de Bourne-Again shell) est un interpréteur en ligne de commande de type script. C'est le shell Unix du projet GNU. (wikipédia)


## Une ligne de commande

![](img/01.png){ width=250px}

- ceci est un `prompt`
- vous pouvez modifier votre prompt (mais ce n'est pas l'objet)
- le votre peut avoir l'air un peu différent

## le prompt

- c'est au niveau de ce prompt que vous saisissez les instructions
- une instruction est une commande ou un programme a executer par l'ordinateur

Par exemple, il existe une commande qui permet de lister le contenu d'un répertoire. 


## le prompt 
\tiny
::: columns
:::: {.column width=.4}


Dans votre terminal

\vspace{.5cm}

```bash
$ /bin/ls
Download Music Media Desktop

$ /bin/ls Music
rihanna.mp3 beyonce.mp3 nirvana.mp3 

$ /bin/pwd
/home/tah
```

::::

:::: {.column width=.6}

- les `/` indiquent la succession de repertoires ou se trouve la commande `ls`
- la commande `ls` permet de lister le contenu d'un répertoire
- sans argument, liste le contenu du répertoire courant (celui ou vous êtes)
- avec argument, `ls` liste le contenu des répertoires donnés en paramètre
- la commande `pwd` permet de connaitre le répertoire courant
- on dit que `/` est le repertoire racine

::::
:::
\normalsize


## les premières commandes
\tiny
::: columns
:::: {.column width=.4}


Dans votre terminal

\vspace{.5cm}

```bash
$ /bin/echo $PATH
/usr/sbin:/usr/bin:/sbin:/bin

$ ls Music
rihanna.mp3 beyonce.mp3 nirvana.mp3 

$ echo $PATH
/usr/sbin:/usr/bin:/sbin:/bin
```

::::

:::: {.column width=.6}

- il est difficile de retenir l'endroit ou se trouve les commandes
- il existe des variables, appelé variable d'environnement qui permettent de se simplifier la vie
- la variable d'environnement `PATH` permet de stocker une liste de répertoires
- quand vous saissisez une commande, le shell parcoure les repertoires listés dans `PATH`
- si la commande se trouve dans l'un de ces répertoires, elle sera executée
- sinon, un message d'erreur apparaîtra 
- on peut regarder le contenu de la variable `PATH` avec la commande `echo`


::::
:::
\normalsize


## les premières commandes
\tiny
::: columns
:::: {.column width=.4}


Dans votre terminal

\vspace{.5cm}

```bash
$ pwd
/home/tah

$ ls  
Download Music Media Desktop

$ cd Music
$ pwd
/home/tah/Music

$ cd ..
$ pwd
/home/tah

$ cd /home/tah/Music
$ pwd
/home/tah/Music
$ ls 
rihanna.mp3 beyonce.mp3 nirvana.mp3 
```

::::

:::: {.column width=.6}

- la commande `cd` permet de changer de répertoire
- on peut changer de répertoire de manière **absolue** ou de manière **relative**
- **absolue** signifie qu'on donne le chemin complet depuis la racine
- **relative** signifie qu'on donne un chemin à partir de notre "position" actuelle
- le répertoire `..` est le répertoire parent
- le répertoire `~` est le répertoire de l'utilisateur
- le répertoire `.` est le répertoire courant

::::
:::
\normalsize


## quelques commandes importantes (1)
\tiny
::: columns
:::: {.column width=.4}


Dans votre terminal

\vspace{.5cm}

```bash
$ man ls
[...]
$ cd Music
$ ls -l
    total 12
    -rw-r--r-- 1 tah tah 2097152 17 août  14:11 beyonce.mp3
    -rw-r--r-- 1 tah tah 3145728 17 août  14:11 nirvana.mp3
    -rw-r--r-- 1 tah tah 4194304 17 août  14:12 rihanna.mp3
$ mkdir rnb
    total 16
    -rw-r--r-- 1 tah tah 2097152 17 août  14:11 beyonce.mp3
    -rw-r--r-- 1 tah tah 3145728 17 août  14:11 nirvana.mp3
    -rw-r--r-- 1 tah tah 4194304 17 août  14:12 rihanna.mp3
    drwxr-xr-x 2 tah tah    4096 17 août  14:13 rnb


```
::::

:::: {.column width=.6}

- la commande `man` permet de lister le manuel d'une commande
- vous pouvez découvrir un nombre important d'option pour chaque commande
- la commande `mkdir` permet de créer un répertoire (rmdir permet de supprimer un répertoire si celui-ci est vide)

::::
:::
\normalsize


## quelques commandes importantes (2)
\tiny
::: columns
:::: {.column width=.4}


Dans votre terminal

\vspace{.5cm}

```bash
$ mv rihanna.mp3 rnb/.
$ ls
beyonce.mp3  nirvana.mp3  rnb
$ ls rnb
rihanna.mp3
$ cp beyonce.mp3 rnb/.
$ ls
beyonce.mp3  nirvana.mp3  rnb
$ ls rnb
beyonce.mp3 rihanna.mp3
$ rm beyonce.mp3
$ ls
nirvana.mp3 rnb


```
::::

:::: {.column width=.6}

- la commande `mv` permet de déplacer et/ou de renommer un fichier
- la commande `cp` permet de copier un fichier (avec l'option -r, on copie un répertoire)
- la commande `rm` permet de supprimer un fichier (avec l'option -r, on supprime un répertoire)

::::
:::
\normalsize



## quelques commandes utiles (1)
\tiny
::: columns
:::: {.column width=.4}

```bash
$ nano
    # editeur de test
$ vim 
    # un meilleur éditeur de texte
$ cat fichier.txt
    # affiche le contenu de fichier.txt
$ 


```
::::

:::: {.column width=.6}

- `nano` est un éditeur de fichier, `vim` aussi, mais en mieux
- `cat` permet d'afficher le contenu d'un fichier

::::
:::
\normalsize



## quelques commandes utiles (2)
\tiny
::: columns
:::: {.column width=.4}

```bash
$ live demo ?

```
::::

:::: {.column width=.6}

- `>` et `<` permettent de rediriger les sorties standard 
- `|` 
- `tail`, `head` permettent d'affichier la fin ou le début d'un ficher
- `find` pour trouver un fichier
- `grep` pour trouver un pattern dans un ou plusieurs fichiers

::::
:::
\normalsize

## quelques commandes utiles (3)
\tiny
::: columns
:::: {.column width=.4}

```bash
$ live demo ?

```
::::

:::: {.column width=.6}

- si une commande n'existe pas, vous pouvez la fabriquer avec une combinaison de commande, ou avec un programme à vous.

::::
:::
\normalsize

